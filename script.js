/*1. Прототипне наслідування 
У кожного обьекта є [[Prototype]] воно дорівню або null або іншому обьекту(прототипу),
коли ми хочемо наприклад прочитати властивість, а її в обьекті немає то JS автоматично шукає цю властивість у прототипі.
Тобто ми використовуємо властивости прототипу для нашого нового обьекту, таким чином ми їх не копіюємо у новий обьект.*/

/*2. super() потрібен для визову батьківського конструктору.*/

class Employee {
    constructor(name, age, salary) {
        this._name = name;
        this._age = age;
        this._salary = salary;
    }

    get name() {
        return this._name;
      }

    set name(value)  {
        this._name = value;
    } 

    get age() {
        return this._age;
      }

    set age(value)  {
        this._age = value;
    } 

    get salary() {
        return this._salary;
      }

    set salary(value)  {
        this._salary = value;
    } 
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    } 

    get salary() {
        return this._salary * 3;
      }

    set salary(value)  {
        this._salary = value;
    }  

}

const alina = new Programmer('Alina',31,100000,'JS');
console.log(alina);

const alla = new Employee('Alla',32,200000,'JS');
console.log(alla);